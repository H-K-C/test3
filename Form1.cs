﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using HalconDotNet;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {



            bool ba = true;

            ba = true;
            ba = false;

            ba &= true;

            //ba &= false;
            int cc = 01;
            HObject ho_image, ho_image_reduce,rectangle;
            HTuple ht_ImagePath = "D:/166999.jpg";


            HOperatorSet.ReadImage(out ho_image, ht_ImagePath);
            //HOperatorSet.GenRectangle1()

          // HOperatorSet.ReduceDomain(ho_image,, out ho_image_reduce);


           // CheckAIImage_LineCheck(ho_image_reduce);


        }


        /// <summary>
        /// 紗線判斷(輸入AI判斷之蝴蝶結區域image)
        /// </summary>
        /// <param name="ho_image">halcon image</param>
        /// <returns> true:判斷為綁線  false:判斷非綁線</returns>
        private bool CheckAIImage_LineCheck (HObject ho_image)
        {
            // HOperatorSet.GenEmptyObj(out ho_Image);

            HTuple ht_Width_Org=null;//影像寬
            HTuple ht_Height_Org = null;//影像高
            HTuple ht_Derivate_Sigma = 11;  // XY方向微分參數
            HTuple ht_Yard_Mean_W = 200; //微分後影像產生之MeanImage之參數
            HTuple ht_Yard_Dyn = 20; //動態閥值分割參數

            HObject ho_Grayimage;  //灰階影像
            HObject ho_DerivGaussImage; //微分後影像
            HObject ho_ScaleMaxImage;//影像對比拉開
            HObject ho_ScaleMaxMeanImage;//
            HObject ho_RegionDynThresh; //動態閥值之絲線
            HObject ho_RegionDynThresh_Dilation; //絲線膨脹
            HObject ho_ConnectedRegions;
            HObject ho_SelectRegions;

            HObject ho_dilationMask; //膨脹遮罩

            //--------初始化
            HOperatorSet.GenEmptyObj(out ho_Grayimage);
            HOperatorSet.GenEmptyObj(out ho_DerivGaussImage);
            HOperatorSet.GenEmptyObj(out ho_ScaleMaxImage);
            HOperatorSet.GenEmptyObj(out ho_ScaleMaxMeanImage);
            HOperatorSet.GenEmptyObj(out ho_RegionDynThresh);
            HOperatorSet.GenEmptyObj(out ho_RegionDynThresh_Dilation);
            HOperatorSet.GenEmptyObj(out ho_ConnectedRegions);
            HOperatorSet.GenEmptyObj(out ho_SelectRegions);
            HOperatorSet.GenEmptyObj(out ho_dilationMask);
            
            try
            {
                //-1--獲得輸入圖像資訊 長/寬
                //HOperatorSet.GetImageSize(ho_image, out ht_Width_Org, out ht_Height_Org);
                //-2--影像轉灰階
                HOperatorSet.Rgb1ToGray(ho_image, out ho_Grayimage);
                //-3--影像微分,銳利化,強化邊緣
                HOperatorSet.DerivateGauss(ho_Grayimage, out ho_DerivGaussImage, ht_Derivate_Sigma, "xy");
                //-4--將影像畫素拉高至0~255區間(強化對比)
                HOperatorSet.ScaleImageMax(ho_DerivGaussImage, out ho_ScaleMaxImage);
                //-5--將圖像平均作為後續動態閥值之對比圖像
                HOperatorSet.MeanImage(ho_ScaleMaxImage, out ho_ScaleMaxMeanImage, ht_Yard_Mean_W, ht_Yard_Mean_W);
                //-6--使用dyn閥值,找尋出可能存在之絲線
                HOperatorSet.DynThreshold(ho_ScaleMaxImage, ho_ScaleMaxMeanImage, out ho_RegionDynThresh, ht_Yard_Dyn, "light");
                //-7--因線段有可能為片段,故需要膨脹
                //-因AI誤判絲線基本上一定是左下往右上傾斜之絲線,故需製作對應之mask(也是左下往右上方向),讓其可連結
                HOperatorSet.GenRectangle2(out ho_dilationMask, 50, 50, 0.43633, 7, 2);   //角度 Rad(25)=0.43633
                //-8--絲線膨脹,讓其連結
                HOperatorSet.Dilation1(ho_RegionDynThresh, ho_dilationMask, out ho_RegionDynThresh_Dilation, 2);
                //-9--Connection 分割片段
                HOperatorSet.Connection(ho_RegionDynThresh_Dilation, out ho_ConnectedRegions);
                //-10--找尋出絲線可能之區域
                HOperatorSet.SelectShape(ho_ConnectedRegions, out ho_SelectRegions, ((((new HTuple("row1")).TupleConcat(
                    "column1")).TupleConcat("row2")).TupleConcat("column2")).TupleConcat("rect2_len1"),
                    "or", ((((((new HTuple(0)).TupleConcat(0)).TupleConcat(ht_Height_Org - 10))).TupleConcat(
                        ht_Width_Org - 10))).TupleConcat(1000), ((((((new HTuple(10)).TupleConcat(10)).TupleConcat(
                            ht_Height_Org))).TupleConcat(ht_Width_Org))).TupleConcat(ht_Width_Org));

                //HOperatorSet.WriteObject(ho_SelectRegions, "D:/01.tiff");
                HTuple num ;
                HOperatorSet.CountObj(ho_SelectRegions, out num);
                //若有找到數量,則判斷為綁線
                if (num.I==0)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch
            {
                return false;

            }
            finally
            {
                 
                ho_Grayimage.Dispose();
                ho_DerivGaussImage.Dispose();
                ho_ScaleMaxImage.Dispose();
                ho_ScaleMaxMeanImage.Dispose();
                ho_RegionDynThresh.Dispose();
                ho_RegionDynThresh_Dilation.Dispose();
                ho_ConnectedRegions.Dispose();
                ho_SelectRegions.Dispose();
                ho_dilationMask.Dispose();
            }
        }



    }

    
    interface Programmer
    {
        string WriteCSharp { get; set; }
      
    }

   


    public class Hua : Programmer
    {
        public string WriteCSharp()
        {
            return "ShitCode";
        }
        public string WriteSQL()
        {
            return "ShitCode";
        }
        public string WriteVB()
        {
            return "ShitCode";
        }
    }

    public class Ming : Programmer
    {
        public string WriteCSharp()
        {
            return "CleanCode";
        }
        public string WriteSQL()
        {
            return "CleanCode";
        }
        public string WriteVB()
        {
            return "CleanCode";
        }
        public Tea MakeTea()
        {
            return new Tea(teaName: "MilkTea");
        }
    }

    public void Work()
    {
        Programmer programmer = new Ming();
        programmer.WriteCSharp(); // "CleanCode"
        programmer.WriteCSharp(); // "CleanCode"
        programmer.WriteCSharp(); // "CleanCode"
    }

    public void NewProject()
    {
        Ming programmer001 = new Ming();
        Programmer programmer002 = new Hua();

  


        programmer001.WriteCSharp(); // "CleanCode"
        programmer002.WriteCSharp(); // "ShitCode"

        programmer001.WriteCSharp(); // "CleanCode"
        programmer002.WriteCSharp(); // "ShitCode"
    }
}
